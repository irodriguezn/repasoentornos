/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repasoentornos;

import java.util.StringTokenizer;

/**
 *
 * @author nacho
 */
public class MetodosEstaticos {
    
    public static void main(String[] args) {
        String primos=primos(10);
        System.out.println(primos);
        int[] primosV=primosV(10);
        System.out.println(muestraV(primosV));
    }
    
    public static boolean esPrimo(int num) {
        boolean esPrimo=true;
        int limite=num/2;
        for (int i=2; i<=limite; i++) {
            if (num%i==0) {
                esPrimo=false;
                break;
            }
        }
        return esPrimo;
    }
    
    public static String primos(int num) {
        String primos="1";
        for (int i=2; i<=num; i++) {
            if (esPrimo(i)) {
                primos+=" " + i;
            }
        }
        return primos;
    }
    
    public static String primosEntre(int inicio, int fin) {
        String primos="";
        for (int i=inicio; i<=fin; i++) {
            if (esPrimo(i)) {
                primos+=i + " ";
            }
            primos=primos.trim();
        }
        return primos;        
    }
    
    
    public static int[] primosVEntre(int inicio, int fin) {
        String primos="";
        for (int i=inicio; i<=fin; i++) {
            if (esPrimo(i)) {
                primos+=i + " ";
            }
            primos=primos.trim();
        }
        int[] primosV=convierteV(primos);
        return primosV;
    }
        
    public static int[] primosV(int num) {
        String primos="1";
        for (int i=2; i<=num; i++) {
            if (esPrimo(i)) {
                primos+=" " + i;
            }
        }
        int[] primosV=convierteV(primos);
        return primosV;
    }
    
    static String muestraV(int v[]) {
        String cad="{";
        for (int e:v) {
            cad+=e + " ";
        }
        cad=cad.trim();
        cad+="}";
        return cad;
    }
    
    static int[] convierteV(String numeros) {
        StringTokenizer st= new StringTokenizer(numeros);
        int tam=st.countTokens();
        int[] v=new int[tam];
        for (int i=0; i<tam; i++) {
            v[i]=Integer.parseInt(st.nextToken());
        }
        return v;
    }
}
