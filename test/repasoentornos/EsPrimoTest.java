/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repasoentornos;

import java.util.Arrays;
import java.util.Collection;
import repasoentornos.MetodosEstaticos;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

/**
 *
 * @author nacho
 */

@RunWith (Parameterized.class)
public class EsPrimoTest {
    int numProbar;
    boolean resultado;

    public EsPrimoTest(int numProbar, boolean resultado) {
        this.numProbar = numProbar;
        this.resultado = resultado;
    }
    
    
   @Parameterized.Parameters
    public static Collection<Object[]> numeros() {
        return Arrays.asList(new Object[][]{
            {7, true},
            {12, false},
            {13, true},
            {19, true}
        });
    }    
    
    @Test
    public void testEsPrimo() {
        assertEquals(resultado, MetodosEstaticos.esPrimo(numProbar));
    }
    
    
}
