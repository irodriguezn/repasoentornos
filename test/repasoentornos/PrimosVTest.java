/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repasoentornos;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import repasoentornos.MetodosEstaticos;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

/**
 *
 * @author nacho
 */

@RunWith (Parameterized.class)
public class PrimosVTest {
    int num;
    int[] resultado;

    public PrimosVTest(int num, int[] resultado) {
        this.num = num;
        this.resultado = resultado;
    }

   @Parameterized.Parameters
    public static List<Object> numeros() {
        return Arrays.asList(new Object[][]{
            {
                7, new int[]{1, 2, 3, 5, 7} 
                    
            }, 
            {
                11, new int[]{1, 2, 3, 5, 7, 11}
                        
            }
        });
    }    
    
    @Test
    public void testEsPrimo() {
        assertArrayEquals(resultado, MetodosEstaticos.primosV(num));
    }
    
    
}
