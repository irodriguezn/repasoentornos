package repasoentornos;

import repasoentornos.MetodosEstaticos;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author nacho
 */
public class MetodosEstaticosTest {
    
    public MetodosEstaticosTest() {
    }

  

    @Test
    public void testEsPrimo() {
        assertEquals(true, MetodosEstaticos.esPrimo(7));
        assertEquals(true, MetodosEstaticos.esPrimo(3));
        assertEquals(true, MetodosEstaticos.esPrimo(5));
        assertEquals(true, MetodosEstaticos.esPrimo(11));
        assertEquals(false, MetodosEstaticos.esPrimo(9));
        assertEquals(false, MetodosEstaticos.esPrimo(24));
    }

    /**
     * Test of primos method, of class MetodosEstaticos.
     */
    @Test
    public void testPrimos() {
        assertEquals("1 2 3 5 7", MetodosEstaticos.primos(10));
    }
    
    @Test
    public void testPrimosV() {
        assertArrayEquals(new int[] {1, 2, 3, 5, 7}, MetodosEstaticos.primosV(10));
    }
}
